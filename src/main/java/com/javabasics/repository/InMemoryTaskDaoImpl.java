package com.javabasics.repository;

import com.javabasics.repository.entity.TasksEntity;

import java.util.Collection;
import java.util.HashMap;

public class InMemoryTaskDaoImpl implements TaskDao {
    private Long id = 0L;
    HashMap<Long, TasksEntity> tasks = new HashMap<>();

    @Override
    public Long save(TasksEntity tasksEntity) {
        tasks.put(++id, tasksEntity);
        return id;
    }

    @Override
    public TasksEntity findById(Long id) {
        return tasks.get(id);
    }

    @Override
    public Collection<TasksEntity> findByUserId(Long userID) {
        return null;
    }
}
