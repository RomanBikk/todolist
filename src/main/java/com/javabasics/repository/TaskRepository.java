package com.javabasics.repository;

import com.javabasics.service.task.model.Task;

public interface TaskRepository {
    Long save (Task task);
    Task findById(long id);
}
