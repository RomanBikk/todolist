package com.javabasics.repository;

import com.javabasics.repository.entity.UserEntity;
import connection.ConnectionFactory;

import java.sql.*;

import static java.lang.String.format;

public class JdbcUserDao implements UserDao {
    private Connection connection = ConnectionFactory.getConnection();
    private ResultSet resultSet;

    private Long id = 0l;
    @Override
    public Long save(UserEntity userEntity) {
        PreparedStatement preparedStatement = null;
        Statement statement = null;
        try {
             statement = connection.createStatement();
            preparedStatement = connection.prepareStatement("INSERT into user( name, password) VALUES(?,?)");//использую плэйсхолдеры.
            preparedStatement.setString(1,userEntity.name);
            preparedStatement.setString(2,userEntity.password);
            preparedStatement.executeUpdate();
            resultSet = statement.executeQuery("SELECT LAST_INSERT_ID() as id");
            resultSet.next();
            return resultSet.getLong("id");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                statement.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;

    }

    @Override
    public UserEntity findById(Long id) {
        UserEntity userEntity = new UserEntity();
        PreparedStatement preparedStatement = null;
        try {
           preparedStatement = connection.prepareStatement("SELECT * FROM user WHERE id=?");
           preparedStatement.setLong(1,id);
           resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
            userEntity.id = resultSet.getLong("id");
            userEntity.name = resultSet.getString("name");
            userEntity.password = resultSet.getString("password");
            }
            else return null;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userEntity;
    }

    @Override
    public UserEntity findByNameAndPassword(String name, String password) {
        UserEntity userEntity = new UserEntity();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM user WHERE name =? AND password=?");
            preparedStatement.setString(1,name);
            preparedStatement.setString(2,password);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                userEntity.id = resultSet.getLong("id");
                userEntity.name = resultSet.getString("name");
                userEntity.password = resultSet.getString("password");
            }
            else return null;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userEntity;
    }
}
