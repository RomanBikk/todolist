package com.javabasics.repository.entity;

import com.javabasics.repository.Column;
import com.javabasics.repository.Id;
import com.javabasics.repository.Table;

import java.util.Objects;
@Table("tasks")
public class TasksEntity {
    @Id
    public Long id;
    public String name;
    @Column("user_id")
    public Long userId;

    public TasksEntity() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TasksEntity that = (TasksEntity) o;
        return name.equals(that.name) &&
                userId.equals(that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, userId);
    }

}
