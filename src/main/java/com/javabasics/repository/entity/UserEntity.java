package com.javabasics.repository.entity;

import com.javabasics.repository.Column;
import com.javabasics.repository.Id;
import com.javabasics.repository.Table;

@Table("user")
public class UserEntity {
    @Id
    public Long id;
    @Column("name")
    public String name;
    public String password;

    public UserEntity() {
    }
}
