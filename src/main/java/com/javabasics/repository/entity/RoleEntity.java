package com.javabasics.repository.entity;

import com.javabasics.repository.Table;

@Table("role")
public class RoleEntity {
    public Long id;
    public String name;

    public RoleEntity() {
    }
}
