package com.javabasics.repository;

import com.javabasics.repository.entity.TasksEntity;
import connection.ConnectionFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class JdbcTaskDao implements TaskDao  {

    private Connection connection = ConnectionFactory.getConnection();
    private ResultSet resultSet;
    @Override
    public Long save(TasksEntity tasksEntity) {
        PreparedStatement preparedStatement = null;
        Statement statement = null;
        try {
            statement = connection.createStatement();
            preparedStatement = connection.prepareStatement("INSERT into tasks (name,user_id) VALUES (?,?)");
            preparedStatement.setString(1, tasksEntity.name);
            preparedStatement.setLong(2, tasksEntity.userId);
            preparedStatement.executeUpdate();
            resultSet = statement.executeQuery("SELECT LAST_INSERT_ID() as id");
            resultSet.next();
            return resultSet.getLong("id");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                statement.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public TasksEntity findById(Long id) {
        TasksEntity tasksEntity = new TasksEntity();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM tasks WHERE id=?");
            preparedStatement.setLong(1,id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                tasksEntity.id = resultSet.getLong("id");
                tasksEntity.name = resultSet.getString("name");
                tasksEntity.userId = resultSet.getLong("user_id");
            }
            else return null;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tasksEntity;
    }

    @Override
    public Collection<TasksEntity> findByUserId(Long userID) {
        List<TasksEntity> tasksEntity = new ArrayList();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM tasks WHERE user_id=?");
            preparedStatement.setLong(1,userID);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                TasksEntity tasksEntity1 = new TasksEntity();
                tasksEntity1.id = resultSet.getLong("id");
                tasksEntity1.name = resultSet.getString("name");
                tasksEntity1.userId = resultSet.getLong("user_id");
                tasksEntity.add(tasksEntity1);
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tasksEntity;
    }
}
