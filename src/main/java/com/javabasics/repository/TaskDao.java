package com.javabasics.repository;

import com.javabasics.repository.entity.TasksEntity;

import java.util.Collection;

public interface TaskDao {
    Long save (TasksEntity tasksEntity);
    TasksEntity findById(Long id);
    Collection<TasksEntity> findByUserId(Long userID);
}
