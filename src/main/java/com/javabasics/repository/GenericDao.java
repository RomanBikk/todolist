package com.javabasics.repository;

import com.javabasics.service.role.model.Role;
import com.javabasics.service.task.model.Task;
import com.javabasics.service.user.model.User;
import connection.ConnectionFactory;
import util.Util;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;

public class GenericDao<T> {
    private Class<?> type;
    private Connection connection = ConnectionFactory.getConnection();


    public GenericDao() {
        type = Util.getGenericTypeClassFromObjectByIndex(this, 0);
    }


    private String getTableName() {

        Table table = type.getAnnotation(Table.class);
        if (table != null) {
            return table.value();
        } else {
            return type.getSimpleName();
        }
    }

    private String getColumnName(Field field) {
        Column column = field.getAnnotation(Column.class);
        if (column != null) {
            return column.value();
        } else {
            return field.getName();
        }
    }


    public Long save(T t) {
        String tableName = getTableName();
        //t.getClass().getSimpleName().replace("Entity","");
        List<String> fieldNames = new ArrayList<>();
        List<String> fieldValues = new ArrayList<>();
        Field[] fields = type.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            if (field.getAnnotation(Id.class) == null) {
                fieldNames.add(getColumnName(field));
                try {
                    fieldValues.add(String.valueOf(field.get(t)));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

        }

        fieldValues = fieldValues.stream()
                .map(value -> "'" + value + "'")
                .collect(Collectors.toList());


        Statement statement = null;

        try {
            statement = connection.createStatement();
            statement.executeUpdate("INSERT into " + tableName + " (" + String.join(",", fieldNames) + ") " + "VALUES" + " (" + String.join(",", fieldValues) + ")");
            ResultSet resultSet = statement.executeQuery("SELECT LAST_INSERT_ID() as id");
            resultSet.next();
            return resultSet.getLong("id");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public T findById(Long id) {
        T t = null;
        Statement statement = null;
        try {
            t = (T) type.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        try {
            Field[] fields = type.getDeclaredFields();
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT  * from " + getTableName() + " WHERE id = " + id);
            rs.next();
            for (Field field : fields) {
                field.set(t, rs.getObject(getColumnName(field)));

            }
        } catch (SQLException | IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


        return t;
    }

}
