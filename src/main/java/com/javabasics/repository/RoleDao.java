package com.javabasics.repository;

import com.javabasics.repository.entity.RoleEntity;

import java.util.Collection;

public interface RoleDao {
    Long save(RoleEntity roleEntity);
    RoleEntity findById(Long id);
    Collection<RoleEntity> findAllRoles();
}
