package com.javabasics.repository;

import com.javabasics.repository.entity.UserEntity;

import java.util.HashMap;

public class InMemoryUserDaoImpl implements UserDao {
    HashMap<Long, UserEntity> users = new HashMap<>();
    private Long id = 0L;
    @Override
    public Long save(UserEntity userEntity) {
        users.put(++id, userEntity);
        return id;
    }

    @Override
    public UserEntity findById(Long id) {
        return users.get(id);
    }

    @Override
    public UserEntity findByNameAndPassword(String name, String password) {
        return null;
    }
}
