package com.javabasics.controller;
import com.javabasics.model.TaskDto;
import com.javabasics.service.task.TaskService;
import com.javabasics.service.task.model.Task;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("todo-list")
public class TaskController {
    private TaskService taskService;
    @PostMapping("save")
public void save(@RequestBody TaskDto task ){
       taskService.save(convertToTask(task));

}

private Task convertToTask(TaskDto taskDto){
        Task task = new Task();
        task.name = taskDto.name;
        return task;
}

}
