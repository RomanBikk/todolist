package com.javabasics.service.task.model;


import com.javabasics.repository.Column;
import com.javabasics.service.user.model.User;

import java.util.Objects;

public class Task {
    public Long id;
    public String name;
    public Long userId;

    public Task(Long id, String name, Long userId) {
        this.id = id;
        this.name = name;
        this.userId = userId;
    }
    public Task() {

    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return  Objects.equals(name, task.name) &&
                Objects.equals(userId, task.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, userId);
    }

    @Override
    public String toString() {
        return "Task name is " + this.name + " user id is " + this.userId;
    }
}
