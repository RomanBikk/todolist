package com.javabasics.service.task;

import com.javabasics.service.task.model.Task;

import java.util.Collection;

public interface TaskService {
    Long save (Task task);
    Task findById(Long id);
    Collection<Task> findByUserId(Long userID);
}
