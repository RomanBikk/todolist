package com.javabasics.service.task;

import com.javabasics.repository.TaskDao;
import com.javabasics.repository.entity.TasksEntity;
import com.javabasics.service.task.model.Task;

import java.util.Collection;
import java.util.stream.Collectors;

public class TaskServiceImpl implements TaskService {
    private TaskDao taskDao;


    public TaskServiceImpl(TaskDao taskDao) {
        this.taskDao = taskDao;
    }


    public Long save(Task task){
        return taskDao.save(convertToTaskEntity(task));
    }

    @Override
    public Task findById(Long id) {
        return  convertToTask(taskDao.findById(id));
    }

    @Override
    public Collection<Task> findByUserId(Long userID) {
        return taskDao.findByUserId(userID)
                .parallelStream()
                .map(taskEntity -> new Task(taskEntity.id, taskEntity.name, taskEntity.userId))
                .collect(Collectors.toList());

    }



    private TasksEntity convertToTaskEntity(Task task) {
        TasksEntity tasksEntity = new TasksEntity();
        tasksEntity.id = task.id;
        tasksEntity.name = task.name;
        tasksEntity.userId = task.userId;
        return tasksEntity;
    }
    private Task convertToTask(TasksEntity tasksEntity) {
        Task task = new Task();
        task.id = tasksEntity.id;
        task.name = tasksEntity.name;
         task.userId = tasksEntity.userId;

        return task;
    }

}

