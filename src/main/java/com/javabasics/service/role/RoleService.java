package com.javabasics.service.role;

import com.javabasics.repository.entity.RoleEntity;
import com.javabasics.service.role.model.Role;

import java.util.Collection;

public interface RoleService {
    Long save (Role role);
    Role findById(Long id);
    Collection<Role> findAllRoles();

}
