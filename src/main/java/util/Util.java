package util;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class Util {
    public static Class<?> getGenericTypeClassFromObjectByIndex(Object o, int genericId) {
        Type genericSuperClass = o.getClass().getGenericSuperclass();
        ParameterizedType parameterizedType = null;
        while (parameterizedType == null) {
            if(genericSuperClass instanceof ParameterizedType) {
                parameterizedType = (ParameterizedType) genericSuperClass;
            }
            else {
                genericSuperClass = ((Class<?>)genericSuperClass).getGenericSuperclass();
            }
        }
        return ((Class<?>)parameterizedType.getActualTypeArguments()[genericId]);
    }

}
