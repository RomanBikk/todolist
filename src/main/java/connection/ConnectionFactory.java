package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionFactory {
    public static Connection getConnection() {
        Connection connection = null;

        try{
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch (ClassNotFoundException e) {
            throw new RuntimeException("Driver was not found");
        }
        try{
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/todolist", "root", "SlowdiveAlison72");
        }
        catch (SQLException e) {
            throw new RuntimeException("Connection was not created");
        }


        return connection;
    }

}
