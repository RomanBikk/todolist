package com.javabasics.service;

import com.javabasics.repository.JdbcRoleDao;
import com.javabasics.repository.RoleDao;
import com.javabasics.service.role.RoleService;
import com.javabasics.service.role.RoleServiceImpl;
import com.javabasics.service.role.model.Role;
import org.junit.Test;

import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RoleServiceImplTest {
    private RoleDao roleDao = new JdbcRoleDao();
    private RoleService roleServiceImpl = new RoleServiceImpl(roleDao);

    @Test
    public void checkReturnedUserIdAfterSaving() {
        Role role = createRole();
        Long insertedId = roleServiceImpl.save(role);
        System.out.println(insertedId);
        assertTrue(isExist(insertedId));
    }

    @Test
    public void roleByIdIsEqualsSavingRole() {
        Role role = createRole();
        Long insertedId = roleServiceImpl.save(role);
        assertEquals(role,roleServiceImpl.findById(insertedId));
    }

    @Test
    public void checkingAllRolesMapping() {
        Collection<Role> roles = roleServiceImpl.findAllRoles();
        for(Role r:roles) {
            System.out.println(r);
        }
    }

    private boolean isExist(Long insertedId) {
        return insertedId != null;
    }
    private Role createRole() {
        Role role = new Role();
        role.name = "Admin" + Math.random()*1000;
        return role;
    }
}
