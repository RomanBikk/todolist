package com.javabasics.service;

import com.javabasics.repository.*;
import com.javabasics.repository.entity.TasksEntity;
import com.javabasics.repository.entity.UserEntity;
import com.javabasics.service.task.model.Task;
import com.javabasics.service.user.model.User;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class JpaTaskDaoTest {
    private UserDao userDao = new JpaUserDao();
    private TaskDao taskDao = new JpaTaskDao();
    //private TaskService taskServiceImpl = new TaskServiceImpl(taskDao);

    @Test
    public void checkReturnedIdAfterSaving() {
        TasksEntity taskEntity = createTaskEntity();
        Long insertedId = taskDao.save(taskEntity);
        assertTrue(isExist(insertedId));
    }
    @Test
    public void checkFindByIdReading() {
        TasksEntity tasksEntity = createTaskEntity();
        Long insertedID = taskDao.save(tasksEntity);
        assertEquals(tasksEntity,taskDao.findById(insertedID));

    }


    private boolean isExist(Long insertedId) {
        return insertedId != null;
    }

    private TasksEntity createTaskEntity() {
        UserEntity userEntity = createUserEntity();
        TasksEntity task = new TasksEntity();
        Long userID = userDao.save(userEntity);
        task.name = "JPA Task Test";
        task.userId = userID;
        return task;
    }
    private UserEntity createUserEntity() {
        UserEntity userEntity= new UserEntity();
        userEntity.name = "Roma"+System.currentTimeMillis();
        userEntity.password = "33443467";
        return userEntity;
    }
}
