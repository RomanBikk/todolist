package com.javabasics.service;

import com.javabasics.repository.*;
import com.javabasics.service.task.TaskService;
import com.javabasics.service.task.TaskServiceImpl;
import com.javabasics.service.task.model.Task;
import com.javabasics.service.user.UserService;
import com.javabasics.service.user.UserServiceImpl;
import com.javabasics.service.user.model.User;
import connection.ConnectionFactory;
import org.junit.Test;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class TaskServiceImplTest {
    private UserDao userDao = new JdbcUserDao();
    private TaskDao taskDao = new JdbcTaskDao();
    private TaskService taskServiceImpl = new TaskServiceImpl(taskDao);
    private UserService userServiceImpl = new UserServiceImpl(userDao);

        @Test
        public void checkReturnedIdAfterSaving() {
            Task task = createTask();
            Long insertedId = taskServiceImpl.save(task);
            assertTrue(isExist(insertedId));
        }

        @Test
        public void taskByIdIsEqualsSavingTask() {
            Task task = createTask();
            Long insertedId = taskServiceImpl.save(task);
            assertEquals(task,taskServiceImpl.findById(insertedId));
        }

        @Test
        public void taskByUserIdIsEqualsSavingTask(){
            User user =  new User();
            Task task = new Task();
            Task task1 = new Task();
            Task task2 = new Task();
            user.name = "Boolean" + System.currentTimeMillis();
            user.password = "Boolean";
            Long userID = userServiceImpl.save(user);
            task.name = "Returning collection test";
            task.userId = userID;
            task1.name = "Returning collection test2";
            task1.userId = userID;
            task2.name = "Returning collection test3";
            task2.userId = userID;
            Long insertedID = taskServiceImpl.save(task);
            Long insertedID2 = taskServiceImpl.save(task1);
            Boolean id1 = false;
            Boolean id2 = false;
            Collection<Task> tasks = taskServiceImpl.findByUserId(userID);
            for (Task t: tasks ) {
                if (t.id == insertedID) {
                    id1 = true;
                }
                if(t.id == insertedID2) {
                    id2 = true;
                }
            }
            assertTrue(id1 && id2);
        }

        private boolean isExist(Long insertedId) {
            return insertedId != null;
        }
        private Task createTask() {
            User user = createUser();
            Task task = new Task();
            Long userID = userServiceImpl.save(user);
            task.name = "My first com.javabasics.service.task";
            task.userId = userID;
            return task;
        }
    private User createUser() {
        User user = new User();
        user.name = "Roma"+System.currentTimeMillis();
        user.password = "33443467";
        return user;
    }



}

