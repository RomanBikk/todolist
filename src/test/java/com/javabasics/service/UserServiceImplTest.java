package com.javabasics.service;

import com.javabasics.repository.*;
import com.javabasics.service.task.TaskService;
import com.javabasics.service.task.TaskServiceImpl;
import com.javabasics.service.user.UserService;
import com.javabasics.service.user.UserServiceImpl;
import com.javabasics.service.user.model.User;
import connection.ConnectionFactory;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UserServiceImplTest {
    private UserDao userDao = new JdbcUserDao();
    private UserService userServiceImpl = new UserServiceImpl(userDao);

    @Test
    public void testConnection() {
        Connection connection = ConnectionFactory.getConnection();
        assertTrue(connection !=null);
    }
    @Test
    public void checkReturnedUserIdAfterSaving() {
        User user = createUser();
        Long insertedId = userServiceImpl.save(user);
        System.out.println(insertedId);
        assertTrue(isExist(insertedId));
    }
    @Test
    public void userbyIdIsEqualSavingUser() {
        User user = createUser();
        Long insertedId = userServiceImpl.save(user);
        assertEquals(user,userServiceImpl.findById(insertedId));

    }
    @Test public void userByNameAndPasswordIsEqualSavingUser() {
        User user = createUser();
        Long insertedID = userServiceImpl.save(user);
        assertEquals(user,userServiceImpl.findByNameAndPassword(user.name,user.password));
    }

    private boolean isExist(Long insertedId) {
        return insertedId != 0;
    }

    private User createUser() {
        User user = new User();
        user.name = "Roma"+Math.random()*1000;
        user.password = "33443467";
        return user;
    }
}
